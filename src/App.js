import React from "react";
import { BrowserRouter, Switch, Link, Route } from "react-router-dom";

import "./App.css";
import Welcome from "./components/welcome/welcome";
import Clock from "./components/clock/clock";
import Contact from "./components/contact/Contact";
import Navigation from "./components/navigation/Navigation";
import NotFound from "./components/notfound/NotFound";
function App() {
  return (
    <BrowserRouter>
      <div id="containerForApp">
        <div className="App">
          <Navigation />
          <Switch>
            <Route exact path="/" component={Welcome} />
            <Route path="/welcome/:name" render={(props) => <Welcome {...props} name={props.match.params.name} />} />
            <Route path="/clock" component={Clock} />
            <Route path="/contact" component={Contact} />
            <Route path="*" component={NotFound} />
          </Switch>
        </div>
      </div>
    </BrowserRouter>
  );
}

export default App;
